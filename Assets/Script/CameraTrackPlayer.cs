﻿/*
 * App : RC Adventure 2D v1
 * Author : Anjar Widiyatmoko
 * 
 * June  5, 2016 : Add Adjustment for Player Position 
 * June 11, 2016 : Set Player Position to n distance from Left Edge of the Screen
 * 				   Fix Aspect Ratio Player problem
 * June 13, 2016 : TODO Add Paralax Effect on Mountain / Forest //
 *  
 */ 
using UnityEngine; 

public class CameraTrackPlayer : MonoBehaviour {

	Transform player;
	GameObject player_go;
	
	GameObject mainCam_go;
	Camera mainCam;

	public float offsetX;
	public float playerSpacer = 2f;
	
	public float screenX;

	// Use this for initialization
	void Start () {
		
		player_go = GameObject.FindGameObjectWithTag ("Player");	// Target to Move by offsetX
		
		mainCam_go = GameObject.FindGameObjectWithTag("MainCamera"); // Movement Reference
		mainCam = mainCam_go.GetComponent<Camera>();
		
		if ( mainCam_go == null ) {
			Debug.Log("CTS.cs:32 : Object Camera Not Found ! ");
		}
		AdjustPlayerPosition();

		if (player_go == null) {
			Debug.LogError ("Couln't find and object 'player'! ");
			return;
		}

		player = player_go.transform;

		//offsetX = transform.position.x - player.position.x;
		
	}
	
	// Update is called once per frame
	void Update () {

		if (player != null) {
			Vector3 pos = transform.position; 
			
			// Screen Adjustment left Player 
			pos.x = player.position.x - offsetX ;
			transform.position = pos ;
		} 
		
	} 
	
	void AdjustPlayerPosition(){
		  
		offsetX  = mainCam.ScreenToWorldPoint( new Vector3( 50f, 
		                                                 Screen.height, 
		                                                 0f)).x; 
	}
	
}
