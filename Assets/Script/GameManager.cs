﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections; 

public class GameManager : MonoBehaviour {

	int playerScores;
	int playerDistance;
	
	Text distanceValue;
	Text scoreValue;
	
	Vector3 startPosX ;
	 
	
	bool Paused = false;
	
	// onGamePaused Menu
//	public GameObject pausePanelMenu;
//	public GameObject crashPanelMenu;
	
	// Alternative Method using GUISkin
	public GUISkin onGameSkin;
	
	GUIText ScoreLabel;
	GUIText DistanceLabel;
	
	// Use this for initialization
	void Start () {
		
		playerDistance = 0; playerScores = 0;
		
		scoreValue =	GameObject.Find("ScoreValue").GetComponent<Text>();
		distanceValue = GameObject.Find("DistanceValue").GetComponent<Text>();
 
		startPosX = transform.position;
		  
	}
	 
	
	// Update GFX
	void FixedUpdate(){
//		DistanceToMeter();
		playerDistance = Mathf.FloorToInt( Vector3.Distance( transform.position, startPosX)*10);
		distanceValue.text = playerDistance.ToString();
	}
	
	void AddScores(){
		playerScores += 10;
		//Debug.Log("Player Score : " + playerScores);
		scoreValue.text = playerScores.ToString();
		
	}
	
	void AddScores(int pts){
		playerScores += pts;
		//Debug.Log("Player Score : " + playerScores);
		scoreValue.text = playerScores.ToString();
	}
	
	void OnTriggerExit2D(Collider2D coll){
	
		if(coll.tag == "Enemy" ) { 
			AddScores();
		} 
	}
	
  
	public void GamePause(string name){
		
		// TODO : Add Feature Switch button Pause to Play if pause Button touched / clicked 
		// 		  Still Buggy how to Hide PausePanel 
		//Debug.Log("Pause Button Clicked ! Text is + " + name  );
		/* TODO : Check Conflickt with PausePanel in UIManager;
		if ( Paused == false ) {
			Time.timeScale = 0f;
			Paused = true; 
			pausePanelMenu.active = true;
		} 
		else {
			Time.timeScale = 1f;
			Paused = false;
			// Set Button Image To Pause
			//playButtton_img.sprite = onGameButton[1];
		} */
	}
	
	void OnGUI() {
		 // TODO : Replace Score / Labelling with GUISkin if Available //
//		ScoreLabel.text = "Your Score : ";
//		DistanceLabel.text = "Your Distance : " ;
		
	}
	
}
