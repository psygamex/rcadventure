﻿using UnityEngine;
using System.Collections;

public class EnemyBuilder : MonoBehaviour {

	public Vector3 velocity = Vector3.zero;
	
	public BoxCollider2D CreateCollider;
	public BoxCollider2D DestroyCollider;
	
	public Rigidbody2D rb;
	
	GameObject mainCam_go;
	Camera mainCam;
	
	GameObject Enemy_go;
	Vector3 Enemy_pos = Vector3.zero;
	
	float collider_offsetX = 5f;
	float collider_offsetY = 0.3f;
	float collider_offset_minY = -1.16f;
	float collider_offset_maxY = 1.8f;
	
	float enemy_offset_minY ;
	float enemy_offset_maxY ;
	int enemy_Max = 2; 		// Max Enemy per Segment -> Front & Backward //
	float maxSpeed = 3f;	// Check with obs_move_left value;
	float enemy_Speed = 1f;
	public int enemy_Num =0;
	bool addEnemy = false;
		 

	
	// Use this for initialization
	void Start () {
		Enemy_go = Resources.Load("Enemy_Default") as GameObject;
		
		// Create Object-Destroyer Collider
		DestroyCollider = GameObject.Find("EnemyBuilder").AddComponent<BoxCollider2D>() as BoxCollider2D;
 		DestroyCollider.offset = new Vector2(-collider_offsetX, collider_offsetY);
		DestroyCollider.size = new Vector2(0.3f,3.5f);
		DestroyCollider.isTrigger = true; 
		
		mainCam  = GameObject.Find("MainCamera").GetComponent<Camera>();
		if ( mainCam == null ) 
			Debug.Log("@EnemyBuiler.cs:45 mainCam Not Found " );
		enemy_offset_minY = mainCam.ScreenToWorldPoint(Vector3.zero).y + 0.5f ;
		enemy_offset_maxY = mainCam.ScreenToWorldPoint(new Vector3(0f,Screen.height - 50 , 0f)).y;
		addEnemy = true; 
		 
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate(){  
		
	}
	
	void OnTriggerEnter2D(Collider2D coll){
		if(coll.tag == "Enemy"   ){
//			Debug.Log("@EnemyBuilder : " + coll.name );
			DestroyEnemy(coll.gameObject); enemy_Num--;
			CreateEnemy();
		} 
	}
	
	void CreateEnemy(){
		int i = 0;
		if ( enemy_Num <= enemy_Max   ) {
			Enemy_pos.x = transform.position.x + Random.Range(collider_offsetX,collider_offsetX+3f);

			Enemy_pos.y = Random.Range(enemy_offset_minY, enemy_offset_maxY);
			//Enemy_go.GetComponent<Rigidbody2D>().velocity = new Vector2(-Random.Range(0,1f),0);
			Instantiate(Enemy_go, Enemy_pos, Quaternion.identity);
			
			enemy_Num++; i++;
		}
		
	}
	
	
	public void DestroyEnemy(GameObject enemy_go){
		Destroy(enemy_go.gameObject);
	}
	
}
