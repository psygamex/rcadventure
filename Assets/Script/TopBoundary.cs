﻿using UnityEngine;
using System.Collections;

public class TopBoundary : MonoBehaviour {

	Camera mainCam;
	GameObject cam_go;
	BoxCollider2D topWall;
	
	//public float wallOffsetX;
	//public float wallOffsetY;
	
	public float sizeX ;
	
	void Start(){
		cam_go= GameObject.Find("MainCamera");
		mainCam = cam_go.GetComponent<Camera>();
		
		if ( mainCam != null ) {
			sizeX = mainCam.ScreenToWorldPoint(new Vector3 ( 0f,0f,0f)).x;
		    GetComponent<BoxCollider2D>().size = new Vector2( mainCam.ScreenToWorldPoint(new Vector3(Screen.width*1.8f ,0f,0f)).x,0.2f);
			
		}
		else {
			Debug.Log("Main Camera Not Found");
		}
		
		           
	}
	
	// Update is called once per frame
	void Update () {
		 
	}
	
}
