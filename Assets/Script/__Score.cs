﻿using UnityEngine;
using System.Collections;

// TODO : Score.cs WillBe Deprecated - Using GameManager
// 

public class Score : MonoBehaviour {

	static int playerScore = 0;
	public GUISkin ScoreSkin;
	
	void Start(){
		
	}
	void OnGUI(){
		GUI.skin = ScoreSkin;
		//GUI.Label ( new Rect(0,0,100,50), playerScore);
		
	}
	
	public static void AddScore(int pts){
		playerScore += pts;
	}
	
	public static int GetScore(){
		return playerScore;
	}
}
