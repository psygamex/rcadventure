﻿/*
 * Updated : 
 * June 4, 2016   : Replaced By GameController.cs
 * 
 */
using UnityEngine;
using System.Collections;

public class RCMovement : MonoBehaviour {

	Vector3 velocity = Vector3.zero;
	float climbSpeed = 50f;
	float forwardSpeed = 1f;

	float maxClimb = 60f;

	bool didClimb = false;
	bool Dead = false;

	float angle = 0f;
	Rigidbody2D rb ;

	
	Touch touch;
	bool paused = false;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	// Do Graphics & Input updates Here
	void Update () {

		if (Dead == true)  
			return;
		
		if (Input.GetKeyDown (KeyCode.Space) ||
		    Input.GetMouseButtonDown (0)   ) {
			
				didClimb = true;
				if ( GetComponent<Rigidbody2D> ().isKinematic == true )
					GetComponent<Rigidbody2D> ().isKinematic = false;
			}
		
		if ( Input.GetKeyDown(KeyCode.Escape) ){
			
			Application.Quit();
		}
		
		if ( Input.touchCount > 0 ) {
				 
				
				if(touch.phase == TouchPhase.Began ) {
					if ( paused == false ) {
						didClimb = true;
				
						if ( GetComponent<Rigidbody2D> ().isKinematic == true )
							GetComponent<Rigidbody2D> ().isKinematic = false;
						paused = true;
						Debug.Log("TouchPhase : Began ");
					}  
			} else if (touch.phase == TouchPhase.Ended ) {
				paused = false;
				Debug.Log("TouchPhase : End " );
			}
		}

	}

	void FixedUpdate () {

		rb = GetComponent<Rigidbody2D> ();

		// Move Forward
		if (rb.velocity.x <= forwardSpeed) {

			rb.AddForce (Vector2.right * forwardSpeed);

		}

		// TODO : 27Nov2016 : Modified & Replace This Touch Climb with up/down Controller //
		if (didClimb) {
			if (rb.velocity.y <= maxClimb) {
				rb.AddForce (Vector2.up * climbSpeed );
			}
			didClimb = false;

		}

		 
	}

	void OnCollisionEnter2D(Collision2D collision){
		
		if (collision.collider.tag == "Ground") {
			Debug.Log ("Collide with Earth");
			GetComponent<Rigidbody2D> ().isKinematic = true;
		}
		if (collision.collider.tag == "Enemy") {
			Dead = true;
			Debug.Log ("Crashed ! ");
		}


	}
 




}
