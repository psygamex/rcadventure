﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SplashLogo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(WaitLogoScene(3));
		//yield return new WaitForSeconds(5);
	}
	
	IEnumerator WaitLogoScene(int time){
		yield return new WaitForSeconds(time);
		SceneManager.LoadScene("SceneMenu");		
	}
	 
}
