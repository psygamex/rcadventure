﻿using UnityEngine; 
using UnityEngine.SceneManagement;


// TODO [ June 14, 2016 ]: Disable Pause button After Crashed 
public class UIManager : MonoBehaviour {

	public GameObject pausePanel;
	public GameObject gameOverPanel;
	public GameObject button_Pause; // Disabled if GameOver state is True //
	 
	public GameObject playerController;
		
	public bool isPaused;		// True if GamePaused by clicked Button 
	public bool isCrashed;		// True if GameOver  
	
	Camera mainCam ;
	
	
	public GameObject go_button_Up;
	public GameObject go_button_Down;
	
	// Player Related
	
	public bool isUp;
	public bool isDown;
	public bool normalize;
	public Rigidbody2D playerRB;
	
	const float climbSpeed = 30f; 
	public float dummyX,dummyY,dummyZ;
	
	Vector3 newPos;
	
	// Use this for initialization
	void Start () {
		
		mainCam = GameObject.Find("MainCamera").GetComponent<Camera>();
		
		isPaused = false; isCrashed = false;
		pausePanel = GameObject.Find("PausePanel");
		gameOverPanel = GameObject.Find("GameOverPanel");
		gameOverPanel.SetActive(false);
		
		playerController = GameObject.Find("Player");
		playerRB =	playerController.GetComponent<Rigidbody2D>();
		
		button_Pause = GameObject.Find("Button_Play");
		if ( button_Pause == null ) {
			Debug.Log("@UIManager.cs : ButtonPause Not Found : ! ");
		}
		button_Pause.SetActive(true);
		
		// Player Related
		go_button_Up = GameObject.Find("Button_Up");
		go_button_Down = GameObject.Find("Button_Down");
		
		newPos = Vector3.zero;
//		Vector3 newBtnUpPos = new Vector3(Screen.width/2, Screen.height/2, 0f);
//		go_button_Up.GetComponent<RectTransform>().transform.position = new Vector3(
//			mainCam.ScreenToViewportPoint(newBtnUpPos).x,
//			mainCam.WorldToScreenPoint(newBtnUpPos).y,
//			mainCam.ViewportToWorldPoint(newBtnUpPos).z);
		//	go_button_Up.transform.position.y);
		//dummyX = mainCam.ScreenToWorldPoint(newBtnUpPos).x;
		dummyX = go_button_Up.GetComponent<RectTransform>().anchoredPosition.x;
		dummyY = go_button_Up.GetComponent<RectTransform>().anchoredPosition.y;
		
			dummyX = go_button_Up.GetComponent<RectTransform>().anchoredPosition.x - 10;
		dummyY = go_button_Up.GetComponent<RectTransform>().anchoredPosition.y;
		
		newPos = new Vector3(dummyX,dummyY,0f);
		      
		go_button_Up.GetComponent<RectTransform>().localPosition= new Vector2(
			go_button_Up.GetComponent<RectTransform>().localPosition.x +10f, 
			go_button_Up.GetComponent<RectTransform>().localPosition.y);
		if ( go_button_Up.GetComponent<RectTransform>() != null ) {
			Debug.Log(go_button_Up.GetComponent<RectTransform>().localPosition.x );
		}
		
		isUp = false;
		isDown = false;
		normalize = false;
	}
	
	// Update is called once per frame
	void Update () {
		if ( isPaused ) {
			PauseGame(true);
		} else {
			PauseGame(false);
		}
		
		if ( isCrashed ) {
			GameOverGame(true);
		} 
		
		// TODO : Need To Sync With GameController.cs 
		if ( Input.GetKeyDown(KeyCode.Escape) && !isCrashed){
			SwitchPause(); 
				
				
		}
		
		
	} 
	
	void FixedUpdate(){
		
		if ( isUp == true ) {
			 
			playerRB.AddForce(Vector2.up * climbSpeed ); 
			isUp = false;
		} 
		if ( isDown == true ) {
			playerRB.AddForce(Vector2.down * climbSpeed ); 			
			isDown = false;
		}
		if ( normalize == true) {
			if ( playerRB.velocity.y > 0f ) playerRB.AddForce(Vector2.down * climbSpeed);
			if ( playerRB.velocity.y < 0f ) playerRB.AddForce(Vector2.up * climbSpeed);
			
			isUp = false;
			isDown = false;
			normalize = false;
		}
	  	
		
	}
	
	void PauseGame(bool state) {
		if ( state == true ) {
			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = 1.0f;
		}
		pausePanel.SetActive(state);
	}
	
	void GameOverGame(bool state){ 
		if ( state == true ) {
			
			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = 1.0f;
		}
		gameOverPanel.SetActive(state);
	}
	

	public void RestartGame(string name){
		// TODO : Better than this ? Replace with any if better.
		SceneManager.LoadScene(name);
	}
	
	public void SwitchPause(){
		if ( isPaused == true ) {
			isPaused = false;
		} else {
			isPaused = true;
		}
	}
	
	public void SwitchGameOver(){
		// TODO : If Game Over -> Button Pause disabled
		
		if ( isCrashed == true ) {
			
			isCrashed = false;
		}else {
				button_Pause.SetActive(false);
				isCrashed = true;
		}
		
	}
	
	public void QuitGame(){
		Debug.Log("[@UIManager:93 ]  Application Quitting ");
		 
		Application.Quit();
	}
	
	// BUTTON UI Section 
	// TODO : [ 26nov2016] Player Movement || Decide wheter in GameController.cs or UIManager.cs
	public void PlayerUp(){
		Debug.Log("[@UIManager:133: ] Player Up");
		isUp = true; isDown = false;
		Debug.Log("@UIManager.cs : BUttonUp x " + mainCam.ScreenToWorldPoint(new Vector3(Screen.width ,Screen.height,0f)).x);
	}
	
	public void PlayerDown(){
		Debug.Log("[@UIManager:139: ] Player Down");
		isUp = false; isDown = true;
		
	}
	
	public void Neutralize(){		//  Neutralize Up/Down Force
		normalize = true;
		Debug.Log("@UIManager:143 Neutralize force");
	}
}

// TODO [OK] : [ 25Nov2016 : Change Control to Button Mode :
//  - Up Down Button
//  - While BUtton not pressed plane steadily flying
