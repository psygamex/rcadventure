﻿using UnityEngine;
using System.Collections;

public class CreateEnemy : MonoBehaviour {

	GameObject enemyHeli;
	Vector3 enemyPos = Vector3.zero;
	float minY = -1.66f;
	float maxY = 1.22f;
	float minX = 9/2.5f;
	float maxX = 9/1.2f;
	
	static int enemyNum = 0;
	int maxEnemy = 2;
	int minEnemy = 1;
	
	bool instantiated = false;
	
	void Start(){
		enemyHeli = Resources.Load("EnemyHeliRed") as GameObject;
	
	}
	void OnTriggerEnter2D(Collider2D collider){
		//Debug.Log("New Enemy Triggered by : " + collider.name ); 
		 
		if ( collider.tag == "Background" ) {
			// TODO Remove this ?
		}
		
	}
	
	public void enableInstantiated(){
		instantiated = false;
	}
	
	void Deploy(){
		// Assign Enemy Positions
		enemyPos.x = GetComponent<BoxCollider2D>().transform.position.x - 2f ;
		enemyPos.y =  Random.Range(minY,maxY);
		//Debug.Log("New Enemy Pos : " + GetComponent<BoxCollider2D>().transform.position.x );
		
		if( instantiated == false && enemyNum <= 15) {
			Instantiate(enemyHeli,enemyPos,Quaternion.identity); enemyNum++;
			enemyPos.x += 4f;//Random.Range(minX,maxX);
				enemyPos.y = Random.Range(minY,maxY); 
			Instantiate(enemyHeli,enemyPos,Quaternion.identity); enemyNum ++;
	 		
		} 
	}
	
	public static void Add (){
		
	}
	
	public static int Count(){
		
		return enemyNum;
	}
	
	
	 
}
