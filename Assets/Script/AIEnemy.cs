﻿using UnityEngine;
using System.Collections;

// TODO [ 22jun2016 ] : Add Feature : If enemy hit player . it must slow down and fall

public class AIEnemy : MonoBehaviour {

	public Vector3 velocity = Vector3.zero;
	Transform enemy;
	float maxSpeed = 3f;
	float speed = 1f;

	Rigidbody2D rb;

	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody2D> ();
		
		
		speed = Random.Range(0f,maxSpeed);
		
		rb.velocity = new Vector2( -speed,0f);
		rb.transform.rotation = new Quaternion(0f,0f,5f,90f);
	}
	
	// Update is called once per frame
	void Update () {
		  
	}
	
	void FixedUpdate() {
		
		rb = GetComponent<Rigidbody2D> ();
		
		// TODO : Inspect enemy obstacles velocity / force again
		if ( Mathf.Abs(rb.velocity.x) <= maxSpeed) {
//			rb.AddForce (Vector2.left * speed );
			rb.velocity = velocity;
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.collider.tag == "Player") {
			rb.gravityScale = 0.2f;
			rb.velocity = Vector3.zero;
				
		}
	}

}
