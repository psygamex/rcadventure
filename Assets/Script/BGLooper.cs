﻿using UnityEngine;
using System.Collections;

public class BGLooper : MonoBehaviour {

	int numBGPanels = 4;
	GameObject collider_go ;
	GameObject enemy_go;

	//public Plane enemyPlane ;
	// TODO : Add Paralax Effect Here regarding with ground / sky 
	void Start(){
		//newEnemy = Resources.Load("EnemyHeliRed") as GameObject;
	}

	void OnTriggerEnter2D(Collider2D collider ) {

//		Debug.Log ("@BGLooper : Triggered : " + collider.name);
 
		if(collider.tag == "Background" || collider.tag == "Ground" ){
			collider_go = collider.gameObject;
			float WidthofBGobject = collider_go.GetComponent<BoxCollider2D>().size.x;
	
			Vector3 pos = collider.transform.position;
			pos.x += WidthofBGobject * numBGPanels - WidthofBGobject ;// / 2f;
	
			collider.transform.position = pos;
			  
		} 
	}
	 
	  
	
}
