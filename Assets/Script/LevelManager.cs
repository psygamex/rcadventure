﻿using UnityEngine;
using UnityEngine.SceneManagement; 

public class LevelManager : MonoBehaviour {
  
	GameObject buttonPlayObject;
	GameObject optionPlayObject;
	GameObject exitPlayObject;
	
	float buttonWidth;
	float buttonHeight;
	
	const float rightMargin = 10f; 
	const float topMargin = 100f;
	float spacer = 15f;
	
	Camera mainCam;
	Vector2 DefaultResolution;
	Vector2 scalingResolution ;
	
	RectTransform recTransform;
	    
	void Start(){
		
		DefaultResolution = new Vector2(480 ,320); // As Scal Factor Ref.
		
		mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>(); 
		
		scalingResolution = new Vector2(Screen.width/DefaultResolution.x, Screen.height/DefaultResolution.y );
		
		// Adjust Position Based On ScreenWidth 
		buttonPlayObject  = GameObject.Find("PlayBtn") ;
		buttonWidth = buttonPlayObject.GetComponent<RectTransform>().rect.width * scalingResolution.x;	// Equal For All button
		buttonHeight = buttonPlayObject.GetComponent<RectTransform>().rect.height * scalingResolution.y;
		
			rescale(buttonPlayObject, buttonPlayObject.GetComponent<RectTransform>()); 	
			SetVectorPosScreen(buttonPlayObject, 
			                   new Vector3(Screen.width - (buttonWidth + rightMargin),
			                               Screen.height - topMargin,0f));
		
		optionPlayObject = GameObject.Find("OptionsBtn");
			rescale(optionPlayObject, optionPlayObject.GetComponent<RectTransform>()); 
			SetVectorPosScreen(optionPlayObject, 
			                   new Vector3(Screen.width - (buttonWidth + rightMargin),
			                               Screen.height - ( topMargin + buttonHeight + spacer ), 0f));
		
		exitPlayObject = GameObject.Find("ExitBtn");
 			rescale(exitPlayObject, exitPlayObject.GetComponent<RectTransform>()); 
			SetVectorPosScreen(exitPlayObject, 
			                   new Vector3(Screen.width - (buttonWidth + rightMargin),
			                               Screen.height - ( topMargin + (2*buttonHeight) + (2*spacer) ),0f));
			
	}
	  
	public void LoadScene(string name ){ 
		SceneManager.LoadScene(name); 
	}
	
	public void QuitGame(){
		Application.Quit(); 
	}
	
	void SetVectorPosScreen(GameObject go, Vector3 pos){
		 go.transform.position = mainCam.ScreenToWorldPoint(pos); 
		 // TODO : Add Scaling For Higher Resolution -> the text in Easy Reading  
		  
	}
	
	void  rescale(GameObject go, RectTransform go_component ) {

		go.GetComponent<RectTransform>().localScale = scalingResolution;

	}
	 
	 
}