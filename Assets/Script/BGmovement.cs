﻿using UnityEngine;
using System.Collections;

public class BGmovement : MonoBehaviour {

	GameObject player_go;
	Rigidbody2D player;
	
	public float speed;
	
	float velocity ;
	
	void Start(){
		player_go = GameObject.FindGameObjectWithTag("Player");
		
		if(player_go == null ){
			Debug.Log("@BGmovement : Couldn't find an object with tag 'Player'! ");
			return;
		}
		
		player = player_go.GetComponent<Rigidbody2D>();
	}
	 
	void FixedUpdate(){
		
		velocity = player.velocity.x *speed;
		
		transform.position = transform.position + Vector3.right * velocity * Time.deltaTime;
	}
}
