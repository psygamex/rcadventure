﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	Vector3 velocity = Vector3.zero;
	float climbSpeed = 50f;
	float forwardSpeed = 1f;

	float maxClimb = 60f;

	bool didClimb = false;
	bool Dead = false;
	bool Landing = false;

	float angle = 0f;
	Rigidbody2D rb ;
	const float gravityPlayer = 0.4f;
 
	Touch touch;
	bool paused = false;
	
	GameObject mainCam_go;
	Camera mainCam;
	
	UIManager uiManager;
	
	public float offsetX;
	public float offsetY;
	
	Transform player ;
	
	// For Debug
	public float camPosX ;
	public float camPosY ;
	
	public float playerVelocity;
	
	// Y coord reference : If Button Clicked do NOT add Climb Effect 
	public float area_Y ;
	
	
	Vector2 scaleRef = new Vector2(480f,320f); 	// Default Resolution as Reference Scaling 
	
	float scalingResolution;
	float scoreLabelPosY ;
	// Use this for initialization
	// Update is called once per frame
	
	void Start () {
		
		player = transform;
		
		rb = GetComponent<Rigidbody2D> ();
		mainCam = GameObject.Find("MainCamera").GetComponent<Camera>();
		uiManager = GameObject.Find("UIManager").GetComponent<UIManager>();
		
		scalingResolution = Screen.height / scaleRef.y;
		scoreLabelPosY = 270f  ;//* scalingResolution;
		
		area_Y =  scoreLabelPosY * scalingResolution ;
		
	}
	
	// Do Graphics & Input updates Here
	void Update () {
		 
		if (Dead == false ){
			
			// By Keyboard Input
			if (  Input.GetKeyDown (KeyCode.Space) ) {
//				Debug.Log("@GetKeyDown.Space : 57 " );
					didClimb = true;
					if ( GetComponent<Rigidbody2D> ().isKinematic == true )
						GetComponent<Rigidbody2D> ().isKinematic = false;
			}
			
			/** 25Nov2016 Temporary Comment
			// By Mouse Click
			else if ( Input.GetMouseButtonDown(0) ){
	
 				Debug.Log("@GetMousBtnDown : 64 : " + Input.mousePosition.y + "vs " + area_Y);
				
				// TODO Resize with Different Resolution
				if ( Input.mousePosition.y  < area_Y ) {
		         	didClimb = true;
		         	if ( GetComponent<Rigidbody2D>().isKinematic == true )
		         		GetComponent<Rigidbody2D>().isKinematic = false;
				} else {
						GetComponent<Rigidbody2D>().isKinematic = false;
				}
//				didClimb = false;
				
				// FIXME [22jun2016] : Fix onGamePanel touch donot add climb effect 
				
	     	} 
	//		TODO Replaced With UIManager.cs Paused Menu Panel 
	//		else if ( Input.GetKeyDown(KeyCode.Escape) ){
	//			 Application.Quit();
	//		} 
	
			// Android Keys 
			else if ( Input.touchCount < 0 ) {
				touch = Input.GetTouch(0);
//				Debug.Log("@Input.Touch : 73 : " + touch.position.y);
				 if(touch.phase == TouchPhase.Began && 
				   touch.position.y < mainCam.ScreenToWorldPoint(new Vector3(0f,area_Y,0f)).y
				  ) {
					if ( paused == false ) {
						didClimb = true;
//						Debug.Log("@GameController.cs : 67 : " + mainCam.ScreenToWorldPoint(new Vector3(0f,Screen.height - 20f,0f)).y );
						if ( GetComponent<Rigidbody2D> ().isKinematic == true )
							GetComponent<Rigidbody2D> ().isKinematic = false;
						paused = true;	// Just delay Paused for a while NOT paused the Game
						 
					}  
				} else if (touch.phase == TouchPhase.Ended ) {
					paused = false;
					 
				}
			}
			
			*/
		}

	}
	 
	// GFX Update
	void FixedUpdate () {
		 
		// rb originial Here //
		playerVelocity = rb.velocity.y;
		// Move Forward
		if (rb.velocity.x <= forwardSpeed) { 
			rb.AddForce (Vector2.right * forwardSpeed); 
		}
		 
		/* REMOVE : Moved To UIManager to control player movement up/down etc 
		if (didClimb) { 
			if (rb.velocity.y <= maxClimb) {
				rb.AddForce (Vector2.up * climbSpeed );
			}
			didClimb = false; Landing = false;
		} */ 
		
		if ( Dead ) {
			// Player down to Earth as Gravityscale
			rb.gravityScale = gravityPlayer;
		}
	}

	// TODO [22jun2016] : Need to sync OnCollisionEnter2D
	void OnCollisionEnter2D(Collision2D collision){
		
		if (collision.collider.tag == "Enemy") {
			Dead = true;
			Debug.Log ("Crashed ! "); 
			if ( Landing == true )
				uiManager.SwitchGameOver();
		} 
		if (collision.collider.tag == "Ground") {
			Landing = true;
//			Debug.Log("@GameController.cs:OnCollissionEnter2D:117 " + collision.collider.tag + Dead.ToString());
			if ( Dead == true ) {
				Time.timeScale = 0f;
//				uiManager.isCrashed = true;
				uiManager.SwitchGameOver();
			} else {
				GetComponent<Rigidbody2D> ().isKinematic = true;
			}
		} 
	} 

	/*	TODO REMOVED ME ! 
	public void MoveUp(){
	 
		
	}
	
	public void MoveDown(){
		// TODO : [ 26 Nov2016 ] Implement MoveDown Player //
	} */
	
}
