﻿using UnityEngine;
using System.Collections;

public class DestroyEnemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		
		Debug.Log("@DestroyEnemy : " + collider.name );
		
		if (collider.tag == "Enemy" ) {
			
			Destroy(collider.gameObject);
			// Add new Enemy
			CreateEnemy.Add();
			Debug.Log(" Count Enemy : " + CreateEnemy.Count());
		}
	}
}
