﻿/*
 * Created by SharpDevelop.
 * User: anjar
 * Date: 6/3/2016
 * Time: 3:04 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */ 
using UnityEngine;
using System.Collections;


public class Plane : MonoBehaviour
{
	GameObject plane_go; 
	Vector3 pos;
	
	void Start()
	{
		plane_go = Resources.Load("EnemyHeliRed") as GameObject;
	}
	
	public void DeployPlane(Vector3 pos){
		Instantiate(plane_go, pos, Quaternion.identity);
	}
		
}